package com.example.onlyme.samsat;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseExpandableListAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Main4Activity extends AppCompatActivity {


    private final static String TAG = "Expandablelistview2Activity";
    /**
     * 定义组数据
     */
    private List<String> groupDataList;
    /**
     * 定义组中的子数据
     */
    private List<List<String>> childDataList;

    private ExpandableListView expandableListView;
    private ExpandableAdapter adapter;

    /**
     * 0：不可选 1：未选中 2：半选 3：选中
     */
    private List<Map<Integer, Integer>> isSelectedList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_main4);
        loadData();
        expandableListView = (ExpandableListView) findViewById(R.id.expandableListView1);

        // expandableListView.setGroupIndicator(getResources().getDrawable(
        // R.drawable.indicator_selector));

        expandableListView.setGroupIndicator(null);
        adapter = new ExpandableAdapter();
        expandableListView.setAdapter((ExpandableListAdapter) adapter);
        expandableListView
                .setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {

                    @Override
                    public void onGroupExpand(int groupPosition) {
                        // 只展开一个
                        for (int i = 0, count = expandableListView.getCount(); i < count; i++) {
                            if (groupPosition != i) {
                                expandableListView.collapseGroup(i);
                            }
                        }
                    }
                });

        expandableListView
                .setOnChildClickListener(new ExpandableListView.OnChildClickListener() {

                    @SuppressLint("LongLogTag")
                    @Override
                    public boolean onChildClick(ExpandableListView parent,
                                                View v, int groupPosition, int childPosition,
                                                long id) {
                        Toast.makeText(
                                Main4Activity.this,
                                "onChildClick===="
                                        + adapter.getChild(groupPosition,
                                        childPosition) + "===="
                                        + v.getParent(), Toast.LENGTH_LONG)
                                .show();
                        ViewHolder viewHolder = (ViewHolder) v.getTag();
                        viewHolder.checkBox.toggle();
                        if (viewHolder.checkBox.isChecked()) {
                            isSelectedList.get(groupPosition).put(
                                    childPosition, 3);
                        } else {
                            isSelectedList.get(groupPosition).put(
                                    childPosition, 1);
                        }
                        int count = 0;
                        for (int i = 0, size = isSelectedList
                                .get(groupPosition).size(); i < size; i++) {
                            if (isSelectedList.get(groupPosition).get(i) == 3) {
                                count++;
                            }
                        }
                        View view = (View) v.getParent();
                        Log.d(TAG, "view=" + view.findViewById(R.id.checkBox1));
                        CheckBox ck = (CheckBox) view
                                .findViewById(R.id.checkBox1);
                        if (count == isSelectedList.get(groupPosition).size()) {
                            // ck.setBackgroundResource(R.drawable.btn_select);
                       //     ck.setButtonDrawable(R.drawable.btn_select);
                        } else if (count > 0) {
                            // ck.setBackgroundResource(R.drawable.btn_half);
                        //    ck.setButtonDrawable(R.drawable.btn_half);
                        } else {
                            // ck.setBackgroundResource(R.drawable.btn_unselect);
                         //   ck.setButtonDrawable(R.drawable.btn_unselect);
                        }
                        adapter.notifyDataSetChanged();
                        return false;
                    }
                });

        isSelectedList = new ArrayList<Map<Integer, Integer>>();
        for (int i = 0, icount = expandableListView.getCount(); i < icount; i++) {
            Map<Integer, Integer> map = new HashMap<Integer, Integer>();
            for (int j = 0, jcount = childDataList.get(i).size(); j < jcount; j++) {
                map.put(j, 1);
            }
            isSelectedList.add(map);
        }
    }

    /**
     * 全部展开或关闭
     *
     * @param flag
     */
    private void showOrhide(boolean flag) {
        for (int i = 0, count = expandableListView.getCount(); i < count; i++) {
            if (flag) {
                expandableListView.expandGroup(i);
            } else {
                expandableListView.collapseGroup(i);
            }
        }
    }

    private void loadData() {
        groupDataList = new ArrayList<String>();
        groupDataList.add("国家");
        groupDataList.add("人物");
        groupDataList.add("武器");

        childDataList = new ArrayList<List<String>>();

        List<String> child1 = new ArrayList<String>();
        child1.add("魏国");
        child1.add("蜀国");
        child1.add("吴国");
        childDataList.add(child1);

        List<String> child2 = new ArrayList<String>();
        child2.add("关羽");
        child2.add("张飞");
        child2.add("典韦");
        child2.add("吕布");
        child2.add("曹操");
        child2.add("甘宁");
        child2.add("郭嘉");
        child2.add("周瑜");
        childDataList.add(child2);

        List<String> child3 = new ArrayList<String>();
        child3.add("青龙偃月刀");
        child3.add("丈八蛇矛枪");
        child3.add("青钢剑");
        child3.add("麒麟弓");
        child3.add("银月枪");
        childDataList.add(child3);
    }

    private class ExpandableAdapter extends BaseExpandableListAdapter {

        @Override
        public int getGroupCount() {
            return groupDataList.size();
        }

        @Override
        public int getChildrenCount(int groupPosition) {
            return childDataList.get(groupPosition).size();
        }

        @Override
        public Object getGroup(int groupPosition) {
            return groupDataList.get(groupPosition);
        }

        @Override
        public Object getChild(int groupPosition, int childPosition) {
            return childDataList.get(groupPosition).get(childPosition);
        }

        @Override
        public long getGroupId(int groupPosition) {
            return groupPosition;
        }

        @Override
        public long getChildId(int groupPosition, int childPosition) {
            return childPosition;
        }

        @Override
        public boolean hasStableIds() {
            return false;
        }

        @SuppressLint("LongLogTag")
        @Override
        public View getGroupView(final int groupPosition, boolean isExpanded,
                                 View convertView, ViewGroup parent) {

            Log.d(TAG, "getGroupView");
            ViewHolder viewHolder = null;
            if (null == convertView) {
                convertView = View.inflate(Main4Activity.this,
                        R.layout.expandablelistview2_groups, null);
                viewHolder = new ViewHolder();
                viewHolder.textView = (TextView) convertView
                        .findViewById(R.id.textView1);
                viewHolder.checkBox = (CheckBox) convertView
                        .findViewById(R.id.checkBox1);
                viewHolder.imageView = (ImageView) convertView
                        .findViewById(R.id.imageView1);
                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }

            viewHolder.textView.setText(groupDataList.get(groupPosition));
            viewHolder.checkBox
                    .setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

                        @Override
                        public void onCheckedChanged(CompoundButton buttonView,
                                                     boolean isChecked) {
                            int flag = 0;
                            if (isChecked) {
                                flag = 3;
                            } else {
                                flag = 1;
                            }
                            for (int i = 0, size = isSelectedList.get(
                                    groupPosition).size(); i < size; i++) {
                                isSelectedList.get(groupPosition).put(i, flag);
                            }
                            notifyDataSetChanged();
                        }
                    });
            int size = 0;
            for (int i = 0, count = isSelectedList.get(groupPosition).size(); i < count; i++) {
                if (isSelectedList.get(groupPosition).get(i) == 3) {
                    size++;
                }
            }
            if (size == isSelectedList.get(groupPosition).size()) {
                // viewHolder.checkBox
                //.setBackgroundResource(R.drawable.btn_select);
               viewHolder.checkBox.setButtonDrawable(R.drawable.ic_call_black_24dp);
            } else if (size > 0) {
                // viewHolder.checkBox.setBackgroundResource(R.drawable.btn_half);
                viewHolder.checkBox.setButtonDrawable(R.drawable.ic_call_black_24dp);
            } else {
                // viewHolder.checkBox
                // .setBackgroundResource(R.drawable.btn_unselect);
                viewHolder.checkBox.setButtonDrawable(R.drawable.ic_call_black_24dp);
            }

            // 判断isExpanded就可以控制是按下还是关闭，同时更换图片
            if (isExpanded) {
                viewHolder.imageView.setBackgroundResource(R.drawable.ic_call_black_24dp);
            } else {
                viewHolder.imageView.setBackgroundResource(R.drawable.ic_call_black_24dp);
            }

            return convertView;
        }

        @SuppressLint("LongLogTag")
        @Override
        public View getChildView(int groupPosition, int childPosition,
                                 boolean isLastChild, View convertView, ViewGroup parent) {

            Log.d(TAG, "getChildView");
            // TextView textView = null;
            // if (null != convertView) {
            // textView = (TextView) convertView;
            // textView.setText(childDataList.get(groupPosition).get(
            // childPosition));
            // } else {
            // textView = createView(childDataList.get(groupPosition).get(
            // childPosition));
            // }
            // return textView;

            ViewHolder viewHolder = null;
            if (null == convertView) {
                convertView = View.inflate(Main4Activity.this,
                        R.layout.expandablelistview2_child, null);
                viewHolder = new ViewHolder();
                viewHolder.textView = (TextView) convertView
                        .findViewById(R.id.textView1);

                viewHolder.checkBox = (CheckBox) convertView
                        .findViewById(R.id.checkBox1);
                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }

            if (isSelectedList.get(groupPosition).get(childPosition) == 3) {
                // viewHolder.checkBox
                // .setBackgroundResource(R.drawable.btn_select);
                // 更改CheckBox的外观
                viewHolder.checkBox.setButtonDrawable(R.drawable.ic_call_black_24dp);
            } else {
                // viewHolder.checkBox
                // .setBackgroundResource(R.drawable.btn_unselect);
                viewHolder.checkBox.setButtonDrawable(R.drawable.ic_call_black_24dp);
            }
            viewHolder.textView.setText(childDataList.get(groupPosition).get(
                    childPosition));
            return convertView;
        }

        @Override
        public boolean isChildSelectable(int groupPosition, int childPosition) {
            return true;
        }

        private TextView createView(String content) {
            TextView textView = null;
            AbsListView.LayoutParams layoutParams = new AbsListView.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT, 80);
            textView = new TextView(Main4Activity.this);
            textView.setLayoutParams(layoutParams);
            textView.setGravity(Gravity.CENTER_VERTICAL | Gravity.LEFT);
            textView.setPadding(80, 0, 0, 0);
            textView.setText(content);
            return textView;
        }

    }

    private class ViewHolder {
        TextView textView;
        CheckBox checkBox;
        ImageView imageView;
    }
}
package com.example.onlyme.samsat;

import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by only me on 02/01/2017.
 */
public class historyfragment extends Fragment{
    @Nullable
    ViewPager viewpager;
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.history,container,false);

        TabLayout tablayout = (TabLayout)v.findViewById(R.id.tablayout);
        tablayout.setTabGravity(TabLayout.GRAVITY_FILL);

        ViewPager viewpager = (ViewPager)v.findViewById(R.id.pager);

        Pager adapter = new Pager(getFragmentManager(),tablayout.getTabCount());
        adapter.addDFragment(new tab1(),"Sedang Berjalan");
        adapter.addDFragment(new tab2(),"Sudah Selesai");
        tablayout.setupWithViewPager(viewpager);
        viewpager.setAdapter(adapter);
        return v;
    }


}

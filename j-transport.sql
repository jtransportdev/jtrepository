-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 10, 2017 at 04:06 PM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `j-transport`
--

-- --------------------------------------------------------

--
-- Table structure for table `antar_samsat`
--

CREATE TABLE IF NOT EXISTS `antar_samsat` (
  `id_order` int(20) DEFAULT NULL,
  `id_driver` int(20) DEFAULT NULL,
  `waktu_pickup` datetime DEFAULT NULL,
  `status` enum('selesai','berjalan') NOT NULL DEFAULT 'berjalan'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE IF NOT EXISTS `customer` (
  `id_customer` int(20) DEFAULT NULL,
  `no_hp` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `driver`
--

CREATE TABLE IF NOT EXISTS `driver` (
  `id_driver` int(20) DEFAULT NULL,
  `nama` varchar(50) DEFAULT NULL,
  `alamat` varchar(100) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `no_hp` varchar(50) DEFAULT NULL,
  `deposit` int(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `kendaraan`
--

CREATE TABLE IF NOT EXISTS `kendaraan` (
  `id_kendaraan` int(20) DEFAULT NULL,
  `id_driver` int(20) DEFAULT NULL,
  `plat` varchar(50) DEFAULT NULL,
  `jenis_kendaraan` varchar(50) DEFAULT NULL,
  `tipe_kendaraan` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `order_car_dk`
--

CREATE TABLE IF NOT EXISTS `order_car_dk` (
  `id_order` int(20) DEFAULT NULL,
  `id_customer` int(20) DEFAULT NULL,
  `lokasi_jemput` varchar(50) DEFAULT NULL,
  `lokasi_tujuan` varchar(50) DEFAULT NULL,
  `jarak` int(20) DEFAULT NULL,
  `ongkos` int(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `order_car_lk`
--

CREATE TABLE IF NOT EXISTS `order_car_lk` (
  `id_order` int(20) DEFAULT NULL,
  `id_customer` int(20) DEFAULT NULL,
  `waktu_order` datetime DEFAULT NULL,
  `lokasi_jemput` varchar(50) DEFAULT NULL,
  `tempat_tujuan` varchar(50) DEFAULT NULL,
  `waktu_berangkat` enum('pagi','siang') DEFAULT NULL,
  `jumlah_penumpang` int(20) DEFAULT NULL,
  `telepon_penumpang` varchar(50) DEFAULT NULL,
  `email_penumpang` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='tempat_tujuan, waktu_berangkat mungkin bisa dibuat tabel baru, pembayaran belum ku buat karena masih bingung';

-- --------------------------------------------------------

--
-- Table structure for table `order_cycle`
--

CREATE TABLE IF NOT EXISTS `order_cycle` (
  `id_order` int(20) DEFAULT NULL,
  `id_customer` int(20) DEFAULT NULL,
  `lokasi_jemput` varchar(50) DEFAULT NULL,
  `lokasi_tujuan` varchar(50) DEFAULT NULL,
  `jarak` int(20) DEFAULT NULL,
  `ongkos` int(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `order_samsat`
--

CREATE TABLE IF NOT EXISTS `order_samsat` (
  `id_order` int(20) DEFAULT NULL,
  `id_customer` int(20) DEFAULT NULL,
  `jenis_order` varchar(50) DEFAULT NULL,
  `lokasi` varchar(50) DEFAULT NULL,
  `jarak` int(20) DEFAULT NULL,
  `ongkos` int(20) DEFAULT NULL,
  `waktu_order` datetime DEFAULT NULL,
  `plat_kendaraan` varchar(50) DEFAULT NULL,
  `nama_pemilik` varchar(50) DEFAULT NULL,
  `pajak` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `payment`
--

CREATE TABLE IF NOT EXISTS `payment` (
  `id_payment` int(20) DEFAULT NULL,
  `id_order` int(20) DEFAULT NULL,
  `rekening` varchar(50) DEFAULT NULL,
  `kode_validasi` varchar(50) DEFAULT NULL,
  `biaya` int(20) DEFAULT NULL,
  `status` enum('selesai','pending') NOT NULL DEFAULT 'pending'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `samsat_dikantor`
--

CREATE TABLE IF NOT EXISTS `samsat_dikantor` (
  `id_order` int(20) DEFAULT NULL,
  `status` enum('selesai','diproses') NOT NULL DEFAULT 'diproses'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `terima_car_dk`
--

CREATE TABLE IF NOT EXISTS `terima_car_dk` (
  `id_order` int(20) DEFAULT NULL,
  `id_driver` int(20) DEFAULT NULL,
  `waktu_pickup` datetime DEFAULT NULL,
  `status` enum('selesai','berjalan') NOT NULL DEFAULT 'berjalan'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `terima_car_lk`
--

CREATE TABLE IF NOT EXISTS `terima_car_lk` (
  `id_order` int(20) DEFAULT NULL,
  `id_driver` int(20) DEFAULT NULL,
  `waktu_pickup` datetime DEFAULT NULL,
  `status` enum('selesai','berjalan') NOT NULL DEFAULT 'berjalan'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `terima_cycle`
--

CREATE TABLE IF NOT EXISTS `terima_cycle` (
  `id_order` int(20) DEFAULT NULL,
  `id_driver` int(20) DEFAULT NULL,
  `waktu_pickup` datetime DEFAULT NULL,
  `status` enum('selesai','berjalan') NOT NULL DEFAULT 'berjalan'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `terima_samsat`
--

CREATE TABLE IF NOT EXISTS `terima_samsat` (
  `id_order` int(20) DEFAULT NULL,
  `id_driver` int(20) DEFAULT NULL,
  `waktu_pickup` datetime DEFAULT NULL,
  `status` enum('selesai','berjalan') NOT NULL DEFAULT 'berjalan'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
